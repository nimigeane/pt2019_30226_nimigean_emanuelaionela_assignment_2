package mainn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import model.Simulation;
import presentation.View;

public class Mainn {

	public static void main(String[] args) {

		View view = new View();
		View.simulationButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				int Q_no = Integer.parseInt(View.Q_noTF.getText());
				int interval = Integer.parseInt(View.IntervalSimTF.getText());
				int TArriveMin = Integer.parseInt(View.ArriveMinTF.getText());
				int TArriveMax = Integer.parseInt(View.ArriveMaxTF.getText());
				int TServiceMin = Integer.parseInt(View.ProcessMinTF.getText());
				int TServiceMax = Integer.parseInt(View.ProcessMaxTF.getText());
				Simulation s = new Simulation(TArriveMin, TArriveMax,TServiceMin, TServiceMax, Q_no, interval);
				s.start();
			
			}
		});
		

	}
}
