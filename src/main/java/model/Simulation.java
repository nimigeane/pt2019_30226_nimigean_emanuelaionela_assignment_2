package model;
import java.util.ArrayList;
import java.util.Random;
import presentation.View;

public class Simulation extends Thread {

	public static int  currentTime;
	public final static int S=2000;
	private int TArriveMin;
	private int TArriveMax;
	private int TProcessMin;
	private int TProcessMax;
	public static int Q_no;
	private static int intSimulation;
	static ArrayList<Queue> queueArr;

	public Simulation(int TArriveMin, int TArriveMax, int TProcessMin, 
			int TProcessMax, int Q_no, int intSimulation) {
		queueArr= new ArrayList<Queue>(Q_no);
		Simulation.Q_no = Q_no;
		Simulation.intSimulation = intSimulation;
		this.TArriveMin = TArriveMin;
		this.TArriveMax = TArriveMax;
		this.TProcessMin = TProcessMin;
		this.TProcessMax = TProcessMax;//procesare
		
		
		for(int i=0; i<Q_no; i++) {
			Queue qe=new Queue(i);
			queueArr.add(qe);
			queueArr.get(i).start();//pornesc thread-ul coada
		}
	}
	
		
	
	private int chooseBestQueue() {
		//aflu indexul cu timpul de asteptare minim, care casa este mai "libera", pt a pozitiona clientul acolo
		int x = 0;
		int min = queueArr.get(0).queueLength();
		for (int i = 1; i < queueArr.size(); i++) {
			int len = queueArr.get(i).queueLength();
			if (len < min) {
				min = len;
				x = i;
			}
		}
		return x;
	}
	
	public String showList() {
		String x="";
		for(int i=0; i<Q_no; i++) {
			x+=queueArr.get(i).toString()+ "Timpul cozii: "+queueArr.get(i).getId_queue()+": "+ queueArr.get(i).getTotalTime()+" \n";
		}
		return x;
	}
	
	public int getTArriveMin() {
		return TArriveMin;
	}

	public void setTArriveMin(int TArriveMin) {
		this.TArriveMin = TArriveMin;
	}

	public int getTArriveMax() {
		return TArriveMax;
	}

	public void setTArriveMax(int TArriveMax) {
		this.TArriveMax = TArriveMax;
	}

	public int getTProcessMin() {
		return TProcessMin;
	}

	public void setTProcessMin(int TProcessMin) {
		this.TProcessMin = TProcessMin;
	}

	public int getTProcessMax() {
		return TProcessMax;
	}

	public void setTProcessMax(int TProcessMax) {
		this.TProcessMax = TProcessMax;
	}

	public int getQ_no() {
		return Q_no;
	}

	public void setQ_no(int Q_no) {
		Simulation.Q_no = Q_no;
	}

	public int getintSimulation() {
		return intSimulation;
	}

	public void setintSimulation(int intSimulation) {
		Simulation.intSimulation = intSimulation;
	}
	
	public ArrayList<Queue> getQ() {
		return queueArr;
	}

	public void setQ(ArrayList<Queue> q) {
		Simulation.queueArr= q;
	}
	}
