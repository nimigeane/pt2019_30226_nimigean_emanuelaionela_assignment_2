package model;

import presentation.View;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ArrayBlockingQueue;


public class Queue extends Thread {

	
	public AtomicInteger total_time=new AtomicInteger(0);
	BlockingQueue<Client> clientsQueue=new ArrayBlockingQueue<Client>(100);
	private int Id_queue, process, wait, finish;
	public final static int S=2000;
	
	public Queue(int Id_queue) {
		setId_queue(Id_queue);
	}

	
	public String toString() {
		String x="";
		for(Client cl: clientsQueue) {
			x+=cl.toString()+" ";
		}
		return x+"\n";
	}
	
	public int getId_queue() {
		return Id_queue;
	}
	
	public void setId_queue(int Id_queue) {
		this.Id_queue = Id_queue;
	}

	public void setClientsQueue(BlockingQueue<Client> clientsQueue) {
		this.clientsQueue = clientsQueue;
	}

	public BlockingQueue<Client> getClientsQueue() {
		return clientsQueue;
	}

	public int getProcess() {
		return process;
	}

	public void setProcess(int process) {
		this.process = process;
	}

	public int getWait() {
		return wait;
	}

	public void setWait(int wait) {
		this.wait = wait;
	}

	public int getFinish() {
		return finish;
	}

	public void setFinish(int finish) {
		this.finish = finish;
	}

	public AtomicInteger getTotalTime() {
		return total_time;
	}

	public void setTotalTime(AtomicInteger totalTime) {
		this.total_time = totalTime;
	}
	
	public int queueLength() {
		return clientsQueue.size();
	}
	
	public void addClient(Client c) {
		clientsQueue.add(c);
	}
	
}