package model;

public class Client {
	
	private int Id_client;
	private int TArrive;
	private int TFinish;
	private int TProcess;
	private int TWait;
	
	public Client(int TArrive, int TProcess) {
		this.TArrive=TArrive;
		this.TProcess=TProcess;
	}
	
	public int getId_client() {
		return Id_client;
	}


	public void setId_client(int id_client) {
		Id_client = id_client;
	}


	public int getTArrive() {
		return TArrive;
	}


	public void setTArrival(int tArrive) {
		TArrive = tArrive;
	}


	public int getTFinish() {
		return TFinish;
	}


	public void setTFinish(int tFinish) {
		TFinish = tFinish;
	}


	public int getTProcess() {
		return TProcess;
	}


	public void setTProcess(int tProcess) {
		TProcess = tProcess;
	}


	public int getTWait() {
		return TWait;
	}


	public void setTWait(int tWait) {
		TWait = tWait;
	}


	public  String toString() {
		return ("Client:" + getId_client()+" ; ");
	}
	
}
