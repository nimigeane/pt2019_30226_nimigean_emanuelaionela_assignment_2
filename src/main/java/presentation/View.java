package presentation;

import java.awt.*;
import javax.swing.*;

public class View{
	
	public static JFrame frame;
	public static JButton simulationButton;
	public static JTextField ArriveMinTF, ArriveMaxTF, ProcessMinTF, ProcessMaxTF, Q_noTF, IntervalSimTF, timer, processTaverage, finishTaverage;
	public static JTextArea resultsView, evolutionQueue;


	public View() {
		
		frame=new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(new Dimension(1350, 880));
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBounds(70,20, 1200, 700);
		
		simulationButton = new JButton("Simulation-START");
		simulationButton.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 16));
		simulationButton.setBounds(100, 380, 200, 50);
		panel.add(simulationButton);
	
		JLabel timerLb = new JLabel("-TIMER-");
		timerLb.setBounds(350, 12, 202, 122);
		timerLb.setFont(new Font("Georgia", Font.ITALIC, 23));
		panel.add(timerLb);
		
		JLabel label = new JLabel("Time - Arriving");
		label.setBounds(72, 92, 202, 122);
		label.setFont(new Font("Georgia", Font.BOLD, 18));
		panel.add(label);
		
		JLabel label1lb = new JLabel("Time - Service");
		label1lb.setBounds(72, 92, 202, 242);
		label1lb.setFont(new Font("Georgia", Font.BOLD, 18));
		panel.add(label1lb);
		
		JLabel label2lb = new JLabel("Number of Queues");
		label2lb.setBounds(72, 92, 202, 362);
		label2lb.setFont(new Font("Georgia", Font.BOLD, 18));
		panel.add(label2lb);
		
		JLabel 	label3lb = new JLabel("Simulation Interval");
		label3lb.setBounds(72, 92, 202, 482);
		label3lb.setFont(new Font("Georgia", Font.BOLD, 18));
		panel.add(label3lb);
		
		JLabel evolutionQ = new JLabel("Evolution of queues");
		evolutionQ.setBounds(470, 400, 400, 300);
		evolutionQ.setFont(new Font("Georgia", Font.BOLD, 17));
		panel.add(evolutionQ);
		
		JLabel res = new JLabel("Results");
		res.setBounds(800, 400, 400, 300);
		res.setFont(new Font("Georgia", Font.BOLD, 17));
		panel.add(res);
		
		JLabel avgProcess = new JLabel("Process time -AVG");
		avgProcess.setBounds(100, 420, 200, 100);
		avgProcess.setFont(new Font("Georgia", Font.BOLD, 12));
		panel.add(avgProcess);
		
		JLabel avgFinish = new JLabel("Finish time -AVG");
		avgFinish.setBounds(100, 480, 200, 100);
		avgFinish.setFont(new Font("Georgia", Font.BOLD, 12));
		panel.add(avgFinish);
		
		ArriveMinTF = new JTextField(20);
		ArriveMinTF.setBounds(250, 130, 50, 30);
		ArriveMinTF.setColumns(10);
		panel.add(ArriveMinTF);

		ArriveMaxTF = new JTextField(20);
		ArriveMaxTF.setBounds(320, 130, 50, 30);
		ArriveMaxTF.setColumns(20);
		panel.add(ArriveMaxTF);

		ProcessMinTF = new JTextField(20);
		ProcessMinTF.setBounds(250, 190, 50, 30);
		ProcessMinTF.setColumns(10);
		panel.add(ProcessMinTF);
		
		ProcessMaxTF = new JTextField(20);
		ProcessMaxTF.setBounds(320, 190, 50, 30);
		ProcessMaxTF.setColumns(10);
		panel.add(ProcessMaxTF);
	
		Q_noTF = new JTextField(20);
		Q_noTF.setBounds(250, 250, 50, 30);
		Q_noTF.setColumns(10);
		panel.add(Q_noTF);
		
		IntervalSimTF = new JTextField(20);
		IntervalSimTF.setBounds(252, 312, 52, 32);
		IntervalSimTF.setColumns(10);
		panel.add(IntervalSimTF);
		
		
		timer=new JTextField();
		timer.setBounds(450, 50, 200, 50);
		timer.setColumns(10);
		panel.add(timer);
		
		processTaverage = new JTextField(20);
		processTaverage.setBounds(100, 480, 200, 30);
		processTaverage.setColumns(10);
		panel.add(processTaverage);
		
		finishTaverage = new JTextField(20);
		finishTaverage.setBounds(100, 540, 200, 30);
		finishTaverage.setColumns(10);
		panel.add(finishTaverage);
		
		evolutionQueue=new JTextArea();
		Font font=new Font("Serif", Font.PLAIN, 15);
		evolutionQueue.setBackground(new Color(153, 133, 120));
		evolutionQueue.setFont(font);
		evolutionQueue.setForeground(Color.WHITE);
		panel.add(evolutionQueue);

		
		JScrollPane scrollPane = new JScrollPane(evolutionQueue);
		scrollPane.setBounds(400, 130, 350, 400);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setVisible(true);
		
		resultsView=new JTextArea();
		resultsView.setBackground(new Color(153, 133, 120));
		resultsView.setFont(font);
		resultsView.setForeground(Color.WHITE);
		
		
		JScrollPane scrollPane1 = new JScrollPane(resultsView);
		scrollPane1.setBounds(780, 130, 350, 400);
		scrollPane1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane1.setVisible(true);
	
		panel.add(scrollPane);
		panel.add(scrollPane1);
		
		
		
		 ImageIcon icon=new ImageIcon("imgBack.jpg");
		 JLabel img=new JLabel(icon);
		 img.add(panel);
		 frame.setContentPane(img);	
		 
		 frame.setVisible(true);
		
	}

	}

